###################################################################################################
# GIT ALIASES
###################################################################################################
alias ga='git add '
alias gb='git branch '
alias gc='git commit '
alias gd='git diff '
alias go='git checkout '
alias gs='git status '
alias gt='git tag '
alias gk='gitk --all&'
alias gx='gitx -all'

###################################################################################################
# STANDARD COMMANDS
###################################################################################################
# Enable color support of ls and also add handy aliases
# Sets colours to distinguish directories from files, etc
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Longlist of files
alias ll='ls -alhF --color=always'

# List all files, including hidden ones
alias ls='ls -lh --color=always'

# Always create parent directories on command
alias mkdir='mkdir -pv'

# Adding prompts to most regular commands
#alias cp='cp -iv'      # interactive, verbose
alias rm='rm -iv'       # interactive, verbose
#alias mv='mv -iv'      # interactive, verbose
alias grep='grep -i'    # ignore case when grepping

# Ensures you always open the Vimproved Version :D
alias vi='vim'

# Auto-resumes wget downloads
alias wget='wget -c'

# Auto-recursive copies
alias cp='cp -r'
alias scp='scp -r'

###################################################################################################
# UBUNTU-SPECIFIC UPDATE/CLEAN COMMANDS
###################################################################################################
# Single update command
alias update='sudo apt update && sudo apt upgrade && sudo apt dist-upgrade'

# Single clean-up command
alias clean='sudo apt autoremove && sudo apt clean all && sudo apt autoclean all'

###################################################################################################
# POWER CONTROLS
###################################################################################################
# Single-word shutdown
alias shutdown='sudo /sbin/shutdown -P now'

###################################################################################################
# CUSTOM ALIASES
###################################################################################################
# Find top 5 largest files
alias findbig="find . -type f -exec ls -s {} \; | sort -n -r | head -5"
