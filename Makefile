# Settings
SHELL := /bin/bash
.ONESHELL:
.PHONY: all anaconda anacondaupdate apt aur awesome archbackup archrecover bash brew cairodock conky docky git i3 openbox pacman pip pipbackup piprecover pipupdate plank rofi sublime themes tint2 touchpad urxvt vim

# Variables
DOTDIR = ${HOME}/dotfiles-universal
LINKCMD = ln -sf

all:

apt:

aur:

awesome:

archbackup: ## Backup arch linux packages
	mkdir -p ${PWD}/arch
	pacman -Qqen > ${PWD}/arch/pacmanlist
	pacman -Qnq > ${PWD}/arch/allpacmanlist
	pacman -Qqem > ${PWD}/arch/aurlist

archfreespace: ## Remove "orphaned" packages to clear up disk space
	sudo pacman -Rns `pacman -Qtdq`

archrecover: ## Recover arch linux packages from backup
	sudo pacman -S --needed `cat ${PWD}/arch/pacmanlist`
	yay -S --needed `cat ${PWD}/arch/aurlist`

bash:
	for file in ".bashrc" ".bash_profile" ".bash_aliases"; do
		if [ -f "$${file}" ]; then
			ln -sf $(DOTDIR)/"$${file}" ${HOME}/"$${file}"
		fi
	done

brew:

cairodock:

# ????
conda: pip
	pip install anaconda

# ????
condaupdate: conda
	conda update anaconda
	conda update --all

conky:

docky:

git:

i3:

openbox:

pacman:

pip:
	mkdir ${HOME}/.local
	pip install --user

pipbackup: ## Backup python packages
	mkdir -p ${PWD}/pip
	pip freeze > ${PWD}/pip/requirements.txt

piprecover: ## Recover python packages
	mkdir -p ${PWD}/pip
	pip install --user -r ${PWD}/pip/requirements.txt

pipupdate: ## Update python packages
	pip freeze | cut -d = -f 1 | xargs pip install -U --user

plank:

rofi:

sublime:

themes:

tint2:

touchpad:

urxvt:

vim:

clean:
	for file in ".bashrc" ".bash_profile" ".bash_aliases"; do
		if [ -f "$${file}" ]; then
			rm -f "$${file}"
		fi
	done
